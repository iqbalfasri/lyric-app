const API_KEY = 'adb4320356ef8660531c9ebcb8b0269e';

const POPULAR_ENDPOIT = () => {
  return (`https://cors-anywhere.herokuapp.com/http://api.musixmatch.com/ws/1.1/chart.tracks.get?page=1&page_size=10&country=id&f_has_lyrics=1&apikey=${API_KEY}`);
}

const TRACK_ENDPOINT = trackId => {
  return (`https://cors-anywhere.herokuapp.com/http://api.musixmatch.com/ws/1.1/track.get?track_id=${trackId}&apikey=${API_KEY}`);
}

const LYRIC_ENDPOINT = trackId => {
  return (`https://cors-anywhere.herokuapp.com/http://api.musixmatch.com/ws/1.1/track.lyrics.get?track_id=${trackId}&apikey=${API_KEY}`);
}

export {
  POPULAR_ENDPOIT,
  TRACK_ENDPOINT,
  LYRIC_ENDPOINT
};