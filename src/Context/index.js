import React, { Component, createContext } from 'react';
import axios from 'axios';

// Api
import { POPULAR_ENDPOIT } from '../Api';

const Context = createContext();

const reducer = (state, action) => {
  switch (action.type) {
    case 'SEARCH_TRACK':
      console.log(state, 'Reducer State');
      console.log(action, 'Reducer Action');
      return {
        ...state,
        data: action.payload
      };

    default:
      console.log(state, 'Reducer State Default Statement')
      return state;
  }
}

class Provider extends Component {
  constructor() {
    super();
    this.state = {
      data: [],

      // Ini membingungkan sekali...
      dispatch: action => {
        this.setState(state => reducer(state, action))
      }

    }
  }

  componentDidMount() {
    // Nanti Fetch Data Disini
    axios.get(POPULAR_ENDPOIT())
      .then(lyric => this.setState({ data: lyric.data.message.body.track_list }))
      .catch(error => console.log(error))
  }

  render() {
    return (
      <Context.Provider value={this.state}>
        {this.props.children}
      </Context.Provider>
    )
  }
}

const Consumer = Context.Consumer;
export {
  Provider,
  Consumer
};