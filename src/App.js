import React, { Component } from 'react';
import {
  Switch,
  Route,
  Link
} from 'react-router-dom';

// Components
import Navbar from './Components/Navbar';
import Footer from './Components/Footer';

// Pages
import Home from './Pages/Home'
import Lyric from './Pages/Lyric';

// Context
import { Provider } from './Context';

class App extends Component {
  render() {
    return (
      <Provider>
        <React.Fragment>
          <Navbar>
            <Link to="/">
              <h1>Lyric App</h1>
            </Link>
          </Navbar>

          <div className="main-container">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/lyric/:trackId" component={Lyric} />
            </Switch>
          </div>
        </React.Fragment>

        <Footer>
          <p>Made by <span style={{ color: '#fb929e' }}>{'</>'}</span> - Iqbal Fasri</p>
        </Footer>
      </Provider>
    );
  }
}

export default App;
