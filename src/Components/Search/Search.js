import React, { Component } from 'react';
import axios from 'axios';
import { Consumer } from '../../Context';

class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      trackTitle: ''
    }
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  onEnterPress = e => {
    if (e.which === 13 || e.keyCode === 13) {
      // this.findTrack;
    }
    return;
  }

  findTrack = (dispatch, e) => {
    e.preventDefault();
    axios.get(`https://cors-anywhere.herokuapp.com/http://api.musixmatch.com/ws/1.1/track.search?q_track=${this.state.trackTitle}&page_size=10&page=1&s_track_rating=des&apikey=adb4320356ef8660531c9ebcb8b0269e`)
      .then(res => {
        dispatch({
          type: 'SEARCH_TRACK',
          payload: res.data.message.body.track_list
        })
      })
      .catch(error => console.log(error))
  }

  render() {
    return (
      <Consumer>
        {context => {
          const { dispatch } = context;
          return (
            <form style={{ padding: 20 }} onSubmit={this.findTrack.bind(this, dispatch)}>
              <input
                name="trackTitle"
                className="search-bar"
                placeholder="Masukan Judul"
                value={this.state.trackTitle}
                onChange={this.handleChange}
                onKeyDown={this.onEnterPress}
              />
            </form>
          )
        }}
      </Consumer>
    )
  }
}

export default Search;
