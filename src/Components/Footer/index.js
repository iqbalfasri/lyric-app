import styled from 'styled-components';

const Footer = styled.div`
  box-sizing: border-box;
  width: 100%;
  height: 50px;
  position: relative;
  background-color: transparent;
  text-align: center;
  font-size: 14px;
  font-weight: lighter;
  color: #333;
  z-index: 888;
  margin: 50px 0 0;
  bottom: 0;
  line-height: 50px;
`

export default Footer;