import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Components
import Loading from '../LoadingIndicators';

// Context
import { Consumer } from '../../Context';

class Tracks extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="d-flex flex-wrap justify-content-center content">
          <Consumer>
            {context => {
              console.log(context)
              const trackList = context.data;

              if (trackList.length === 0 || trackList === undefined) {
                return <Loading />;
              }

              return (
                trackList.map(track => (
                  <div className="col-md-6" key={track.track.track_id}>
                    <div className="box">
                      <h1>{track.track.track_name}</h1>
                      <p className="text-muted">by - {track.track.artist_name}</p>
                      <Link to={`/lyric/${track.track.track_id}`} className="main-button">Lihat Lirik</Link>
                    </div>
                  </div>
                ))
              );

            }}
          </Consumer>
        </div>
      </React.Fragment>
    )
  }
}

export default Tracks;