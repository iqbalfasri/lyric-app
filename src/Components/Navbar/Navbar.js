import styled from 'styled-components';

const Navbar = styled.div`
  width: 100%;
  padding: 20px;
  background-color: #fff;
  box-shadow: 1px 1px 8px rgba(0, 0, 0, 0.1);
  text-align: center;
  font-size: 24px;
  font-weight: 600;
  color: #111;
  position: fixed;
  top: 0;
  z-index: 888;
`

export default Navbar;