
class Sort {
  constructor(num_list) {
    this.l = bubbleSort(num_list);
  }
  put(num) {
    this.l = insert(this.l, num)
  }
  getList() {
    return this.l;
  }

}
function insert(num_list, num) {
  num_list.push(num);
  let i = num_list.length - 1;
  let tmp = num_list[i];

  while (i > 0 && tmp < num_list[i - 1]) {
    num_list[i] = num_list[i - 1];
    i = i - 1;
  }
  num_list[i] = tmp;

  return num_list;
  /*let newArr = [];
  let inserted = false;
  let len = num_list.length;
  for (let i = 0; i < len; i++) {
      if (!inserted && num_list[i] >= num) {
          console.log('yey', inserted);
          inserted = true;
          newArr.push(num) //masukan array ketika index >= num
      }
      newArr.push(num_list[i])
  }
  if (!inserted)
      newArr.push(num);
  return newArr*/
}
function bubbleSort(num_list) {
  var len = num_list.length;
  for (var i = 0; i < len; i++) {
    for (var j = 0; j < len + i - 1; j++) {
      if (num_list[j - 1] > num_list[j]) {
        var temp = num_list[j - 1];
        num_list[j - 1] = num_list[j];
        num_list[j] = temp;
      }
    }
  }
  return num_list
}
let arr1 = [1, 2];
let arr2 = [3, 4];

let num_list = [1, 4, 25, 16, 9];
let sorter = new Sort(num_list);
console.log(sorter.getList());
sorter.put(3);
console.log(sorter.getList());
sorter.put(10);
console.log(sorter.getList());

console.log('------------------------------||---------------------------------');

class Merger {

  constructor() {
    this.ll = []
  }

  addList(l) {
    let isArray = l && typeof l === 'object' && l.constructor === Array;
    if (!isArray)
      throw new TypeError('Only array is acceptable.');
    this.ll.push(l);
  }
  /*    let maxLen =0;
                  for(let i=0 ; i< this.ll.length; i++){
                      if(this.ll[i].length >maxLen)
                          maxLen =this.ll[i].length;
                  }*/
  getMergedList() {
    let mergedList = [];

    for (let j = 0; ; j++) {
      //   console.log('column = ' + j);
      let count = 0;
      // console.log('count kembali ke 0 = ' + count)
      for (let i = 0; i < this.ll.length; i++) {
        if (this.ll[i][j] !== undefined) {
          mergedList.push(this.ll[i][j]);
          count++;
          // console.log('row = ', i);
          //console.log('push =' + this.ll[i][j] + ' count =', count);
        }
      }
      if (count == 0)
        break;
    }
    return mergedList
  }
}

m = new Merger();
m.addList([1, 4, 5,]);
m.addList([3, 2, 4, 5, 6, 7]);

console.log(m.getMergedList());     // [ 1 ]

m = new Merger();
m.addList(['g']);
m.addList(['a']);
console.log(m.getMergedList());     // [ 'g', 'a' ]

m = new Merger();
m.addList(['x', 'y']);
m.addList(['a', 'b', 'c']);
console.log(m.getMergedList());     // [ 'x', 'a', 'y', 'b', 'c' ]

m = new Merger();
m.addList(['a', 'k']);
m.addList(['x', 'h']);
console.log(m.getMergedList());     // [ 'a', 'x', 'k', 'h' ]


m = new Merger();
m.addList([1, 2, 3,]);
m.addList([4, 5]);
m.addList([6, 7, 8, 9]);
console.log(m.getMergedList());     // [ 1, 4, 6, 2, 5, 7, 3, 8, 9 ]

console.log('------------------------------||---------------------------------');
// Case-sensitive search
// Never use indexOf() or slice()


function findIndex(sentence, keyword) {
  SENTENCE_LOOP:
  for (let i = 0; i < sentence.length - keyword.length + 1; i++) {
    for (let j = 0; j < keyword.length; j++) {
      if (sentence[i + j].toLowerCase() !== keyword[j].toLowerCase())
        continue SENTENCE_LOOP;
    }
    return i;
  }
  return null;
}

function findPeople(peopleDB, keyword) {
  let searchResult = [];
  for (let i = 0; i < peopleDB.length; i++) {
    let count = 0;
    let newArr = [];
    // console.log('ayo cari di kota = ' +peopleDB[i].area)
    let newNames = peopleDB[i].names
    for (let j = 0; j < peopleDB[i].names.length; j++) {

      let index = findIndex(newNames[j], keyword)
      //  console.log('           findIndex ( ', newNames[j],',', keyword,' )');
      if (index != null) {
        count++
        newArr.push(newNames[j]);
      }
    }
    // console.log('jumlah di area ',peopleDB[i].area, ' = ',count)
    if (count != 0)
      searchResult.push({ area: peopleDB[i].area, names: newArr })
    //  console.log('masukan kedalam search reasult', searchResult)

  }
  // console.log('---------------')
  return searchResult;
}

let peopleDB = [
  { area: 'Jakarta', names: ['IWAN Park', 'hALIM Jason', 'John Park', 'CHalim', 'Mike Kim'] },
  { area: 'Surabaya', names: ['Halim Super', 'Galaxy S9', 'iwan iPhone SS'] },
  { area: 'Yogyakarta', names: ['Gunwoo Park', 'Halim Hahaha', 'NICE Job Bro'] },
];

console.log(findPeople(peopleDB, 'lim'));
console.log('-----');
console.log(findPeople(peopleDB, 'iwan'));
console.log('---------');
console.log(findPeople(peopleDB, 'PARK'));
console.log('-----');
console.log(findPeople(peopleDB, 'ob br'));
console.log('--------------------------------------||---------------------------------');


class WordCounter {

  constructor() {
    this.counter = {};      // e.g. {'the': 5, 'what': 4, 'i': 4, ...}

  }

  addSentence(sentence) {
    let words = sentence.split(' ');
    for (let i = 0; i < words.length; i++) {
      // console.log('current words ['+i+'] =',words[i])
      let word_i = words[i].toLowerCase();
      let newString = '';
      for (let j = 0; j < words[i].length; j++) {
        if ('a' <= word_i[j] && word_i[j] <= 'z' || '0' <= word_i[j] && word_i[j] <= '9') {
          newString += word_i[j]
        }
      }
      if (this.counter[newString]) {
        this.counter[newString]++;
      } else {
        this.counter[newString] = 1

      }
    }
  }

  getRanking(top = null) {
    let counterAsArray = Object.keys(this.counter).map((word) => {
      return [word, this.counter[word]];
    });
    counterAsArray.sort((first, second) => {
      return second[1] - first[1];
    });
    if (top)
      counterAsArray = counterAsArray.splice(0, top);
    return counterAsArray;
  }
}

let wordCounter = new WordCounter();
// wordCounter.addSentence('We are the future.');
// wordCounter.addSentence('Hey, what is the matter?');
// wordCounter.addSentence('Hey! I do not know what you are saying.');
// wordCounter.addSentence('I do not know what you are saying~!!!');
// wordCounter.addSentence('I am 20 years old.....');
// wordCounter.addSentence('We went to the hospital yesterday. 20 times!');
// wordCounter.addSentence('The old timer is somewhat weird.');
// wordCounter.addSentence('Live or die.... No future in this life. No Matter What!!!');
// wordCounter.addSentence('I used to live in Jakarta!');
wordCounter.addSentence('The city was weird. Weird. So weird!');
console.log('-----');
console.log(wordCounter.getRanking(5));
console.log('-----');
console.log(wordCounter.getRanking());
console.log('--------------------------------------||---------------------------------');

console.log('UPDATE OBJECT ARRAY');

function updateObjectArray(objArr, key, checkVal, updateObj) {
  for (let i in objArr) {
    //console.log(i);
    // console.log(objArr[i][key])
    //console.log('CheckVal=', checkVal(objArr[i][key]))
    //console.log('UpdateObj=',updateObj(objArr[i][key]))
    if (objArr[i][key] && checkVal(objArr[i][key])) {
      updateObj(objArr[i]);
    }
  }
}

let students = [
  { name: 'Jhon Park', height: 185, age: 20 },
  { name: 'iwan', height: 180, age: 20 },
];


updateObjectArray(students, 'height', (val) => val > 182, (obj) => obj['age']++);
console.log(students);
updateObjectArray(students, 'name', (val) => val.length > 6, (obj) => obj['longName'] = true);
console.log(students);
updateObjectArray(students, 'age', (val) => val <= 20, (obj) => obj['name'] += 'Hihi');
console.log(students);


function randomString(length, chars) {
  var result = '';
  for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
}
console.log("----------------------------------")
console.log(randomString(200, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'))

function bikinobjek(cek) {
  console.log(cek)
  let rv = {};
  for (let i = 0; i < cek.length; i++) {
    console.log('cek[i][j]', cek[i][1])
  }
  return rv
}
let category = [[1, 'satu'], [2, 'dua'], [3, 'tiga']];
console.log(bikinobjek(category))

var customIndexOf = function (arrayLike, searchElement) {
  var object = Object(arrayLike);
  var length = object.length >>> 0;
  var fromIndex = arguments.length > 2 ? arguments[2] >> 0 : 0;
  if (length < 1 || typeof searchElement !== 'string' || fromIndex >= length) {
    return -1;
  }

  if (fromIndex < 0) {
    fromIndex = Math.max(length - Math.abs(fromIndex), 0);
  }

  var search = searchElement.toLowerCase();
  for (var index = fromIndex; index < length; index += 1) {
    if (index in object) {
      var item = object[index];
      if (typeof item === 'string' && search === item.toLowerCase()) {
        return index;
      }
    }
  }

  return -1;
};

var names = [
  'John',
  'Anne',
  'anne',
  'Brian'
];
console.log('...')
console.log(customIndexOf(names, 'aNnE'));
console.log('...')

var customIndexOf = function (array, searchElement, fromIndex) {
  return array.map(function (value) {
    return value.toLowerCase();
  }).indexOf(searchElement.toLowerCase(), fromIndex);
};
console.log(customIndexOf(names, 'aNnE'));

let catArr = [
  [1, 'Management'],
  [2, 'IT/Software'],
  [3, 'IT/Hardware'],
];

function findIndexOf(sentence, keyword) {
  SENTENCE_LOOP:
  for (let i = 0; i < sentence.length - keyword.length + 1; i++) {
    for (let j = 0; j < keyword.length; j++) {
      if (sentence[i + j].toLowerCase() !== keyword[j].toLowerCase())
        continue SENTENCE_LOOP;
    }
    return i;
  }
  return null;
}

function findCat(catArr, keyword) {

  let searchResult = [];
  let count = 0;
  for (let i = 0; i < catArr.length; i++) {
    let index = findIndexOf(catArr[i][0], keyword)

    if (index != null) {
      count++
      searchResult.push(catArr[i]);
    }
  }
  return searchResult;
}
console.log(findCat(catArr, 1));


function findGF(arr, value) {
  return arr[value - 1][1]
}

console.log(findGF(catArr, 3));
// { name: 'cherries', quantity: 5 }

let numArr = [1000, 2000000, 3100000000];

function nFormatter(num) {
  if (num >= 1000000000) {
    return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
  }
  if (num >= 1000000) {
    return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
  }
  if (num >= 1000) {
    return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
  }
  return num;
}

function getNum(num) {
  let newNum = []
  for (let i = 0; i < num.length; i++) {
    newNum.push(nFormatter(num[i]));
  }
  num = newNum
  return num

}

console.log(getNum([
  {
    _bizcat_name: 'Financial services and products',
    _cnt_comment: 44111,
    _cnt_like: 33111111,
    _cnt_report: 0,
  },
  {
    _bizcat_name: 'Financial services and products',
    _cnt_comment: 44111,
    _cnt_like: 33111111,
    _cnt_report: 0,
  },
  {
    _bizcat_name: 'Financial services and products',
    _cnt_comment: 44111,
    _cnt_like: 33111111,
    _cnt_report: 0,
  }
]))

myArray = ['a', 'b', 'c', 'd']
myArray.splice(1, 1)
console.log(myArray)

function parseComment(text) {
  let parsed = [];
  while (true) {
    let stag = text.indexOf('<<@@<<');
    if (stag === -1) {
      parsed.push([text, false]);
      break;
    }
    let etag = text.indexOf('>>@@>>', stag);
    if (stag > 0)
      parsed.push([text.slice(0, stag), false]);
    parsed.push([text.slice(stag + 6, etag), true]);
    text = text.slice(etag + 6);
  }
  return parsed;
}

console.log(parseComment('<<@@<<Iwan Park>>@@>>Who are you? <<@@<<Halim Kim>>@@>>   Hello'));
console.log(parseComment('We need listen to<<@@<<Andre Hong>>@@>>\'s voice'));
console.log(parseComment('Nonton Avengers in @CGV <<@@<<HO HO HA HA>>@@>>'));