import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

// API
import { TRACK_ENDPOINT, LYRIC_ENDPOINT } from '../Api';

// Components
import Loading from '../Components/LoadingIndicators';

export default class Lyric extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lyric: [],
      tracks: [],
      lyricBody: ''
    }

    this.trackId = props.match.params.trackId;
  }

  componentDidMount() {
    // document.title = `Lirik - ${this.trackId}`;
    // const url = `https://cors-anywhere.herokuapp.com/http://api.musixmatch.com/ws/1.1/track.lyrics.get?track_id=${this.state.tracks.track_id}&apikey=adb4320356ef8660531c9ebcb8b0269e`;
    // axios.get(url)
    //   .then(res => {
    //     // const lyricBody = res.data.message.body.lyrics.lyrics_body;
    //     console.log(url, 'Isi Lirik');
    //     console.log(res.data.message, 'Data Message');
    //     this.setState({ lyricBody: res.data.message.body.lyrics });
    //     console.log(this.trackId, 'Track ID');
    //     return axios.get(`https://cors-anywhere.herokuapp.com/http://api.musixmatch.com/ws/1.1/track.get?track_id=${this.trackId}&apikey=adb4320356ef8660531c9ebcb8b0269e`);
    //   })
    //   .then(res => this.setState({ tracks: res.data.message.body.track }, () => console.log(this.state.tracks.track_id, 'State of Tracks')))
    //   .catch(error => console.log(error))
    this.getDetailLyric();
  }


  componentWillUnmount() {
    document.title = 'Home';
  }

  getDetailLyric = () => {
    const TRACK_URL = TRACK_ENDPOINT(this.trackId);
    const LYRIC_URL = LYRIC_ENDPOINT(this.state.tracks.track_id);

    axios.get(TRACK_URL)
      .then(res => {
        const track = res.data.message.body.track;
        this.setState({ tracks: track }, () => console.log(this.state.tracks.track_id, 'trek id'));

        return axios.get(LYRIC_URL);
      })
      .then(res => {
        const lyric = res.data.message.body;
        console.log(lyric);
        this.setState({ lyricBody: lyric });
      })
      .catch(error => console.log(error))
  }

  render() {
    return (
      <div>
        <div>
          <h1 className="popular-text">
            <Link to="/"><i className="fas fa-long-arrow-alt-left"></i></Link>
            {this.state.tracks.track_name}
          </h1>
        </div>
        <div className="d-flex flex-wrap justify-content-center align-items-center">
          <pre style={{ width: '100%', textAlign: 'center' }}>
            {this.state.lyricBody}
          </pre>
        </div>
      </div>
    )
  }
}
