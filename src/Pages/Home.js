import React, { Component } from 'react';

// Components
import Tracks from '../Components/Tracks';
import Search from '../Components/Search';

class Home extends Component {
  render() {
    return (
      <div>
        <h1 className="popular-text">
          Lagu Terpopuler
        </h1>
        <Search />
        <Tracks />
      </div>
    )
  }
}

export default Home;